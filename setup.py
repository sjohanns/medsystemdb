#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
from setuptools import setup

setup(
    name="Knowlerdge and Data Base for Medical expert system",
    version="0.0.1",
    license="MIT",
    author="Johann Tovstolyak",
    author_email="tovstolaki62@gmail.com",
    description="It's a DB and KB for Medical Diagnostics Expert System",
    packages=['medsystemdb'],
    platforms='unix',
    install_requires=[
        "blinker",
        "click",
        "Flask",
        "gunicorn",
        "itsdangerous",
        "Jinja2",
        "MarkupSafe",
        "packaging",
        "Werkzeug"
    ]
)
