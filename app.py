
# Flask
from flask import Flask, flash, render_template, request, \
    redirect, url_for, session, jsonify
from forms import RegistrationForm, LoginForm
from werkzeug.security import generate_password_hash, check_password_hash
#from flask_login import LoginManager

# Standart libraty
import json
import hashlib
import time
import datetime
import itertools
# Lib

from lib.database import DB

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'a4f3ffae2645839d37191092cff801f7e48c1db4'
app.permanent_session_lifetime = datetime.timedelta(days=10)
#login_manager = LoginManager(app)

class Users(DB):
    def __init__(self):
        self.name = 'users'
        DB.__init__(self)
    def getItemFromDB(self, keys=None, filters=[]):
        response = []
        tags = list(itertools.zip_longest(keys, filters))
        print(keys)
        if 'name' in keys:
            tmp = [tag[1][1] for tag in tags if tag[0] == 'name' and tag[1][0] == 'equal']
            if tmp:
                oldData = self.data
                for t in tmp:
                    data = self.data[t[0]]
                    print(t, filters)
                    self.data = data
                    response += DB.getItemFromDB(self, keys=set(keys), filters=filters)
                    self.data = oldData
        else:
            oldData = self.data
            for key, value in oldData:
                self.data = value
                response = DB.getItemFromDB(self, keys=set(keys), filters=filters)
#                print(response)
            self.data = oldData
        return response
    def addToDB(self, new_data, key=None):
        if key:
            self.data[new_data['name'][0]][key] = new_data
            try:
                with open(self.path + self.name + '.json', 'w', encoding='utf8') as f:
                    json.dump(self.data, f, indent=2, ensure_ascii=False)
                    return True
            except IOError as e:
                print(e)
                return False
        else:
            return 'No key'
class Diseases(DB):
    def __init__(self):
        self.name = 'diseases'
        DB.__init__(self)
class Patients(DB):
    def __init__(self):
        self.name = 'patients'
        DB.__init__(self)

dbs = {
    "users": Users(),
    "diseases": Diseases(),
    "patients": Patients()
    }

users = None
@app.before_request
def before_request():
    global users
    with open('data/users.json', 'r') as db:
        users = json.load(db)

#@app.after_request
#def after_request():
#    session['logged'] = True

@app.route('/')
def root():
    return render_template("root.html")
    # return redirect(url_for('/login'), 301)


@app.route('/registration', methods=['GET', 'POST'])
def registration():
    form = RegistrationForm()
    if request.method == 'POST':
        if request.form:
            if request.form['password'] == request.form['confirm']:
                key = hashlib.sha256(request.form['username'].encode())
                key = key.hexdigest()
                if not dbs['users'].data.get(key):
                    user = {
                        'name': request.form['username'],
                        'work': request.form['work'],
                        'pwd': generate_password_hash(request.form['password']),
                        'time': time.ctime()
                    }
                    res = dbs['users'].addToDB(user, key=key)
                    if res:
                        session['logged'] = True
                        flash('You were successfully registrated in', 'msg')
                        return redirect(url_for("root"), 301)
                else:
                    session['logged'] = False
                    flash('User exist in Data Base', 'error')
            else:
                session['logged'] = False
                flash('Confirm need be as Password', 'error')
        else:
            flash('Form is empty', 'error')
    return render_template('registration.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
    session['logged'] = False
    session.permanent = True
    form = LoginForm()
    if request.method == 'POST':
        user = dbs['users'].getItemFromDB(
            keys=['name'],
            filters=[('equal', request.form['username'])])
        if user:
            if check_password_hash(user['pwd'], request.form['password']):
                flash('You were successfully logged!', 'msg')
                session['logged'] = True
                return redirect(url_for("root"))
            else:
                flash('Incorrect password!!', 'error')
                session['logged'] = False
                return redirect(url_for("login"))
        else:
            flash('Incorrect username or you were not registred!!', 'error')
            session['logged'] = False
            return redirect(url_for("login"), 301)
    else:
        if 'logged' in session:
            if session['logged']:
                flash('User is ready logged !', 'msg')
                return redirect(url_for('root'), 301)
            else:
                return render_template('login.html', form=form)
        else:
            session['logged'] = False
            session.modified = True
            return render_template('login.html', form=form)


@app.route('/api/<db>/<username>/<password>/', methods=['GET', 'POST'])
def api(db, username, password):
    msg = '\n***********DataBase_Info************'
    if request.method == 'GET':
        req = dbs['users'].getItemFromDB(keys=['name', 'pwd'], filters=[('equal', username)])
        if req:
            if check_password_hash(req[0][1], password):
                msg += '\nUser is logged successfully'
                if request.args.get('db_values'):
                    return jsonify(list(dbs[db].data[0].values()))
                elif request.args.get('db_keys'):
                    return jsonify(list(dbs[db].data.keys()))
                else:
                    try:
                        keys = (request.args.get('keys')).split(',')
                        if request.args.get('filters'):
                            filters = (request.args.get('filters') or '').split(',')
                            filters = [(filt.removesuffix(')')).split('(') for filt in filters]
                        else:
                            filters = []
                        req = dbs[db].getItemFromDB(keys=keys, filters=filters)
                    except Exception as e:
                        return jsonify({'error': repr(e)})
                if request.args.get('mode') == 'json':
                    return jsonify(req)
                else:
                    msg += '\nReturn: \n%s' % (req)
            else:
                msg += '\nIncorrect password'
        else:
            msg += "\nUser isn't logged. Please you are logging here /regestration"
    return msg, '200'
