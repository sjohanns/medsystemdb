
import json
import itertools
from copy import copy

class DB():
    filters = {'equal': (lambda a, b: b in a), 'more': (lambda a, b: a > b),
               'less': (lambda a, b: a  < b)}
    def __init__(self, name = '', path='./data/'):
        self.name = name or self.name
        self.path = path
        try:
            with open(self.path + self.name + '.json', 'r', encoding="ascii") as f:
                self.data = json.load(f)
        except IOError as e:
            print(e)
    def getData(self):
        return self.data
    def getItemFromDB(self, keys=None, filters=[]):
#        print(self.data)
        response = []
        tags = itertools.zip_longest(keys, filters)
        for tag in tags:
            truth = []
            for value in self.data.values():
                if not filters or not tag[1]:
                    truth.append(value.get(tag[0]))
                else:
                    if self.filters[tag[1][0]](tag[1][1], value.get(tag[0]) or ''):
                        truth.append(value.get(tag[0]))
                        continue
                    else:
                        truth.append(False)
            response.append(truth)
        result = []
        for i in range(len(response[0])):
            cur = []
            for j in range(len(response)):
                cur.append(response[j][i])
            result.append(cur)
        return [res for res in result if all(res)]


    def reWriteDB(self, data):
        self.data = data
        try:
            with open(self.path + self.name + '.json', 'w', encoding='utf8') as f:
                json.dump(data, f, indent=2, ensure_ascii=False)
            return True
        except IOError as e:
            print(e)
            return False
    def addToDB(self, new_data, key=None):
        if key:
            self.data[key] = new_data
            try:
                with open(self.path + self.name + '.json', 'w', encoding='utf8') as f:
                    json.dump(self.data, f, indent=2, ensure_ascii=False)
                    return True
            except IOError as e:
                print(e)
                return False
        else:
            return 'No key'
    def __repr__(self):
        return 'DataBase(%s): %s' % (self.name, self.path)
