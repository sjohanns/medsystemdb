
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, SelectField
from wtforms.validators import InputRequired, EqualTo

class RegistrationForm(FlaskForm):
    username = StringField('Username: ', [InputRequired()])
    password = PasswordField('Password: ', [InputRequired(), EqualTo('confirm',
                                                                   message='Password must match')])
    confirm = PasswordField('Confirm Password: ', [InputRequired()])
    work = SelectField('Your work: ', choices=[('expert', 'Expert'),
                                               ('patient', 'Patient')], validators=[InputRequired()])
    submit = SubmitField('Registration')
class LoginForm(FlaskForm):
    username = StringField('Username: ', [InputRequired()])
    password = PasswordField('Password: ', [InputRequired()])
    submit = SubmitField('Login')
